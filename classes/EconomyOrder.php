<?php
namespace classes;
/**
 * Создатель типа доставки "эконом"
 */
class EconomyOrder extends TaxCall
{
   private $model, $price;

   public function __construct($model, $price) {
      $this->model = $model;
      $this->price = $price;
   }

   public function getCar(): DeliveryType {
      return new EconomyCab($this->model, $this->price);
   }

}
?>