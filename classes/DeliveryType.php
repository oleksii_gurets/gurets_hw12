<?php
namespace classes;
/**
 * Общий интерфейс для продукта(объекта) для имплементации классами-создателями
 */
interface DeliveryType
{
   public function getModel();
   public function getPrice();
   public function getMessage();
}
?>