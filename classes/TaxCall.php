<?php
namespace classes;
/**
 * Абстрактный класс-создатель, содержащий фабричный метод getCar
 */
abstract class TaxCall 
{
   abstract public function getCar(): DeliveryType;

   public function getOrder() {
      $car = $this->getCar();
      $car->getMessage();
   }
}
?>