<?php
namespace classes;
/**
 * Создатель типа доставки "люкс"
 */
class LuxOrder extends TaxCall
{
   private $model, $price;

   public function __construct($model, $price) {
      $this->model = $model;
      $this->price = $price;
   }

   public function getCar(): DeliveryType {
      return new LuxCab($this->model, $this->price);
   }

}
?>