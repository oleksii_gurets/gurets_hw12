<?php
namespace classes;
/**
 * Продукт типа "эконом", класс, имплементирующий интерфейс продукта, возращающий объект продукта
 */
class EconomyCab implements DeliveryType
{
   private $model, $price;

   public function __construct($model, $price) {
      $this->model = $model;
      $this->price = $price;
   }

   public function getModel() {
      return $this->model;
   }

   public function getPrice() {
      return $this->price;
   }

   public function getMessage() {
      echo 'Your cab order is done! You have chosen economy class cab!' . '</br>';
      echo 'The cab model is ' . $this->getModel() . '</br>';
      echo 'The cab ride price is ' . $this->getPrice() . '</br>';
   }

}
?>