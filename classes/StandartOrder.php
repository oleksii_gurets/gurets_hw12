<?php
namespace classes;
/**
 * Создатель типа доставки "стандарт"
 */
class StandartOrder extends TaxCall
{
   private $model, $price;

   public function __construct($model, $price) {
      $this->model = $model;
      $this->price = $price;
   }

   public function getCar(): DeliveryType {
      return new StandartCab($this->model, $this->price);
   }

}
?>