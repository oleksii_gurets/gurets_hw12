<?php
spl_autoload_register(function ($className) {
   $filePath = __DIR__ . "/" . str_replace("\\", "/", $className) . ".php";
   if(file_exists($filePath)) {
      require_once str_replace("\\", "/", $className) . ".php";
   } else {
      throw new Exception("Невозможно загрузить $className.");
   }
});

use \classes\TaxCall;
use \classes\EconomyOrder;
use \classes\StandartOrder;
use \classes\LuxOrder;


function clientOrder(TaxCall $order) {
   $order->getOrder();
}

clientOrder(new EconomyOrder('nissan', 15));
echo '</br>';
clientOrder(new StandartOrder('ford', 30));
echo '</br>';
clientOrder(new LuxOrder('porche', 250));
echo '</br>';
clientOrder(new EconomyOrder('opel', 20));
?>